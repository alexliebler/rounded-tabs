import { useState } from "react"

export const useTabs = () => {
  const [activeTab, setActiveTab] = useState(0);

  return {
    activeTab,
    setActiveTab
  }
}