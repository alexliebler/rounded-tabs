import React from "react"
import { useTabs } from "../../hooks/use-tabs/use-tabs"
import classNames from "classnames";

export const TabGroup = ({children}) => {
  const {activeTab, setActiveTab} = useTabs();

  return (
    <div className="tab-group">
      <ul className="tab-list">
        {React.Children.map(children, (child, i) => (
          <li
            key={`Tab ${i}`}
            className={classNames({
              'tab': true,
              'tab--active': i === activeTab
            })}
            onFocus={() => setActiveTab(i)}
            onClick={() => setActiveTab(i)}
            tabIndex={0}
          >
            {child.props.title}
            <div className="tab-border" />
          </li>
        ))}
      </ul>
      <div className="tab-content">
        {children[activeTab]}
      </div>
    </div>
  )
}