import "./styles.css";
import { Tab } from "./components/tab/tab";
import { TabGroup } from "./components/tab-group/tab-group";

export default function App() {
  return (
    <div className="App">
      <TabGroup>
        <Tab title="hello">What's up?</Tab>
        <Tab title="doumo">Genki desu ka?</Tab>
        <Tab title="buenos dias">muy bien?</Tab>
      </TabGroup>
    </div>
  );
}
